# please specify working directory
# working_directory = r'M:\Dropbox\luhk_work\programming\python\FQC_FluxQualityControl'

import os
# os.chdir(working_directory)

# automatic detection of working directory, test if running on RDS
# automatically detect folder where main.py resides
# currently not working on RDS, but works on local computer
abspath = os.path.abspath(__file__)
working_directory = os.path.dirname(abspath)
os.chdir(working_directory)

from func import please_quality_control
please_quality_control(working_directory)

