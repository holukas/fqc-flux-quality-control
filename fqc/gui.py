from tkinter import *
import webbrowser

def main_window(site_list, IRGA_list):

    def link_callback(event):
        webbrowser.open_new(r"http://envsupport.licor.com/help/EddyPro3/Content/Topics/Despiking_Raw_Stat_Screening.htm")

    master = Tk()
    master.wm_title("FQC - ETH Flux Quality Control - QC_FINAL_FLAG")

    site_to_process = StringVar()
    select_IRGA = StringVar()
    eddypro_qc = IntVar()
    minimum_threshold = IntVar()
    maximum_threshold = IntVar()
    window_dirtiness = IntVar()
    spikes_hf = IntVar()
    amplitude_resolution_hf = IntVar()
    drop_out_hf = IntVar()
    absolute_limits_hf = IntVar()
    skewness_kurtosis_hf = IntVar()
    discontinuities_hf = IntVar()
    timelag_hf = IntVar()
    timelag_sf = IntVar()
    attack_angle_hf = IntVar()
    non_steady_wind_hf = IntVar()
    agc_threshold = IntVar()
    agc_setting = StringVar()
    flag_H = IntVar()
    fill_gaps = IntVar()
    ustar_hf = IntVar()

    # set default values
    site_to_process.set('Please select site ...')
    select_IRGA.set('Please select used IRGA ...')
    eddypro_qc.set(1)
    minimum_threshold.set(1)
    maximum_threshold.set(1)
    window_dirtiness.set(1)
    spikes_hf.set(1)
    amplitude_resolution_hf.set(0)
    drop_out_hf.set(1)
    absolute_limits_hf.set(1)
    skewness_kurtosis_hf.set(0)
    discontinuities_hf.set(0)
    timelag_hf.set(0)
    timelag_sf.set(0)
    attack_angle_hf.set(0)
    non_steady_wind_hf.set(0)
    agc_threshold.set(80)
    agc_setting.set('flag AGC values above')
    flag_H.set(1)
    fill_gaps.set(1)
    ustar_hf.set(0)

    master.geometry('1500x706+150+200')

    Label(master, justify=RIGHT, text="CHOOSE SITE")\
        .grid(column=0, row=0, sticky=W)
    OptionMenu(master, site_to_process, *site_list)\
        .grid(column=1, row=0, sticky=W)

    Label(master, justify=RIGHT, text="CHOOSE IRGA")\
        .grid(column=0, row=1, sticky=W)
    OptionMenu(master, select_IRGA, *IRGA_list)\
        .grid(column=1, row=1, sticky=W)

    Label(master, text="Please select quality tests:")\
        .grid(column=0, row=2, sticky=W)

    Checkbutton(master, text="STEADY STATE & TURBULENCE", variable=eddypro_qc)\
        .grid(column=1, row=2, sticky=W)
    Label(master, text="(eddypro_qc) SSITC: steady state test and developed turbulent conditions test (see Foken et al., 2004; Foken and Wichura, 1996; Goeckede et al., 2008)")\
        .grid(column=2, row=2, sticky=W)

    Checkbutton(master, text="MINIMUM FLUX", variable=minimum_threshold)\
        .grid(column=1, row=3, sticky=W)
    Label(master, text="(minimum_threshold) acceptable minimum flux values")\
        .grid(column=2, row=3, sticky=W)
    Checkbutton(master, text="MAXIMUM FLUX", variable=maximum_threshold)\
        .grid(column=1, row=4, sticky=W)
    Label(master, text="(maximum_threshold) acceptable maximum flux values")\
        .grid(column=2, row=4, sticky=W)

    Checkbutton(master, text="AGC", variable=window_dirtiness)\
        .grid(column=1, row=5, sticky=W)
    Label(master, text="(raw data screening) (window_dirtiness) automatic gain control threshold, signal strength")\
        .grid(column=2, row=5, sticky=W)
    agc_setting_list = ('flag AGC values above', 'flag AGC values below')
    OptionMenu(master, agc_setting, *agc_setting_list)\
        .grid(column=2, row=5, sticky=E)
    Entry(master, textvariable=agc_threshold)\
        .grid(column=3, row=5, sticky=W)

    Checkbutton(master, text="SPIKE TEST", variable=spikes_hf)\
        .grid(column=1, row=6, sticky=W)
    Label(master, text="(raw data screening) (spikes_hf)")\
        .grid(column=2, row=6, sticky=W)

    Checkbutton(master, text="AMPLITUDE RESOLUTION", variable=amplitude_resolution_hf)\
        .grid(column=1, row=7, sticky=W)
    Label(master, text="(raw data screening) (amplitude_resolution_hf)")\
        .grid(column=2, row=7, sticky=W)

    Checkbutton(master, text="DROP-OUTS", variable=drop_out_hf)\
        .grid(column=1, row=8, sticky=W)
    Label(master, text="(raw data screening) (drop_out_hf)")\
        .grid(column=2, row=8, sticky=W)

    Checkbutton(master, text="ABSOLUTE LIMITS", variable=absolute_limits_hf)\
        .grid(column=1, row=9, sticky=W)
    Label(master, text="(raw data screening) (absolute_limits_hf) for input data wind, sonic temperature, concentration values")\
        .grid(column=2, row=9, sticky=W)

    Checkbutton(master, text="SKEWNESS & KURTOSIS", variable=skewness_kurtosis_hf)\
        .grid(column=1, row=10, sticky=W)
    Label(master, text="(raw data screening) (skewness_kurtosis_hf)")\
        .grid(column=2, row=10, sticky=W)

    Checkbutton(master, text="DISCONTINUITIES", variable=discontinuities_hf)\
        .grid(column=1, row=11, sticky=W)
    Label(master, text="(raw data screening) (discontinuities_hf) automatic gain control threshold, signal strength")\
        .grid(column=2, row=11, sticky=W)

    Checkbutton(master, text="TIME LAGS HARD FLAG", variable=timelag_hf)\
        .grid(column=1, row=12, sticky=W)
    Label(master, text="(timelag_hf)")\
        .grid(column=2, row=12, sticky=W)

    Checkbutton(master, text="TIME LAGS SOFT FLAG", variable=timelag_sf)\
        .grid(column=1, row=13, sticky=W)
    Label(master, text="(timelag_sf) automatic gain control threshold, signal strength")\
        .grid(column=2, row=13, sticky=W)

    Checkbutton(master, text="ANGLE OF ATTACK", variable=attack_angle_hf)\
        .grid(column=1, row=14, sticky=W)
    Label(master, text="(raw data screening) (attack_angle_hf)")\
        .grid(column=2, row=14, sticky=W)

    Checkbutton(master, text="STEADINESS OF HORIZONTAL WIND", variable=non_steady_wind_hf)\
        .grid(column=1, row=15, sticky=W)
    Label(master, text="(raw data screening) (non_steady_wind_hf)")\
        .grid(column=2, row=15, sticky=W)

    # Checkbutton(master, text="SENSIBLE HEAT FLAG", variable=flag_H).grid(column=1, row=16, sticky=W)
    # Label(master, text="(flag_H) set flag for H to 2 when all CO2, H2O and LE fluxes are flagged with 2").grid(column=2, row=16, sticky=W)

    Checkbutton(master, text="FILL GAPS", variable=fill_gaps)\
        .grid(column=1, row=17, sticky=W)  # todo
    Label(master, text="fill missing values between start and end date with -9999")\
        .grid(column=2, row=17, sticky=W)

    Checkbutton(master, text="USTAR FILTER", variable=ustar_hf)\
        .grid(column=1, row=18, sticky=W)
    Label(master, text="set flag to 2 if u* is below site-specific limit")\
        .grid(column=2, row=18, sticky=W)

    lbl_google_link = Label(master, text="Help", fg="Blue", cursor="hand2")
    lbl_google_link\
        .grid(column=2, row=99, sticky=E)
    lbl_google_link.bind("<Button-1>", link_callback)

    Button(master, text='Run', command=master.quit)\
        .grid(column=1, row=99, sticky=W, padx=10, pady=10)
    # Button(master, text='Exit', command=master.quit).grid(column=2, row=99, sticky=W, pady=4)

    master.mainloop()
    master.destroy()

    site_to_process = site_to_process.get()
    select_IRGA = select_IRGA.get()

    eddypro_qc = eddypro_qc.get()
    minimum_threshold = minimum_threshold.get()
    maximum_threshold = maximum_threshold.get()
    window_dirtiness = window_dirtiness.get()

    spikes_hf = spikes_hf.get()
    amplitude_resolution_hf = amplitude_resolution_hf.get()
    drop_out_hf = drop_out_hf.get()
    absolute_limits_hf = absolute_limits_hf.get()
    skewness_kurtosis_hf = skewness_kurtosis_hf.get()
    discontinuities_hf = discontinuities_hf.get()
    timelag_hf = timelag_hf.get()
    timelag_sf = timelag_sf.get()
    attack_angle_hf = attack_angle_hf.get()
    non_steady_wind_hf = non_steady_wind_hf.get()

    agc_threshold = agc_threshold.get()
    agc_setting = agc_setting.get()

    # flag_H = flag_H.get()
    fill_gaps = fill_gaps.get()

    ustar_hf = ustar_hf.get()

    flag_list_of_all_selected = []  # list that will contain the names of all selected flags

    # STANDARD FLAGS ----------------------------------------
    if eddypro_qc == 1:
        flag_list_of_all_selected.append('eddypro_qc')
    if minimum_threshold == 1:
        flag_list_of_all_selected.append('minimum_threshold')
    if maximum_threshold == 1:
        flag_list_of_all_selected.append('maximum_threshold')
    if window_dirtiness == 1:
        flag_list_of_all_selected.append('window_dirtiness')
    if ustar_hf == 1:
        flag_list_of_all_selected.append('ustar')

    # STATISTICAL FLAGS -------------------------------------
    flag_list_9_digits = []
    if spikes_hf == 1:
        flag_list_9_digits.append('spikes_hf')
        flag_list_of_all_selected.append('spikes_hf')
    if amplitude_resolution_hf == 1:
        flag_list_9_digits.append('amplitude_resolution_hf')
        flag_list_of_all_selected.append('amplitude_resolution_hf')
    if drop_out_hf == 1:
        flag_list_9_digits.append('drop_out_hf')
        flag_list_of_all_selected.append('drop_out_hf')
    if absolute_limits_hf == 1:
        flag_list_9_digits.append('absolute_limits_hf')
        flag_list_of_all_selected.append('absolute_limits_hf')
    if skewness_kurtosis_hf == 1:
        flag_list_9_digits.append('skewness_kurtosis_hf')
        flag_list_of_all_selected.append('skewness_kurtosis_hf')
    if discontinuities_hf == 1:
        flag_list_9_digits.append('discontinuities_hf')
        flag_list_of_all_selected.append('discontinuities_hf')

    flag_list_5_digits = []
    if timelag_hf == 1:
        flag_list_5_digits.append('timelag_hf')
        flag_list_of_all_selected.append('timelag_hf')
    if timelag_sf == 1:
        flag_list_5_digits.append('timelag_sf')
        flag_list_of_all_selected.append('timelag_sf')

    flag_list_2_digits = []
    if attack_angle_hf == 1:
        flag_list_2_digits.append('attack_angle_hf')
        flag_list_of_all_selected.append('attack_angle_hf')
    if non_steady_wind_hf == 1:
        flag_list_2_digits.append('non_steady_wind_hf')
        flag_list_of_all_selected.append('non_steady_wind_hf')

    # (EddyPro qc_ is searched automatically)

    return eddypro_qc, minimum_threshold, maximum_threshold, window_dirtiness, agc_setting, agc_threshold, ustar_hf,\
           site_to_process, flag_list_9_digits, flag_list_5_digits, flag_list_2_digits, select_IRGA
