"""

    !!! REMINDER:
        SSITC flag in FLUXNET file may not bet changed
        instead, set measured values to -9999 in case additional QC checks
        are performed on the FLUXNET data before EFDC upload

"""

from tkinter import *

import numpy as np
import pandas as pd
from matplotlib import gridspec


def please_quality_control(working_directory):
    version = 'FQC 2.1.2'

    # 0 = original full_output and FLUXNET header (FOF: 3 rows, 1st row is ignored, FLUXNET: 1 row)
    # 1 = merged full_output and FLUXNET header (FOF: 2 rows, FLUXNET: 1 row)
    header_type = 0

    # if the files were merged, then the merged FLUXNET file might have a different date format
    # and index column
    FLXNET_file_dateformat = '%Y%m%d%H%M'
    # FLXNET_file_dateformat = '%Y-%m-%d %H:%M:%S'
    # fxn_index_col=0
    fxn_index_col = 1

    # VARIABLES -------------------------------------
    FOF_qc_variable_list = ['co2_flux', 'H', 'LE', 'h2o_flux', 'ch4_flux',
                            'n2o_flux']  # searching for these variables in full_output (EddyPro column names)
    FLXNT_qc_variable_list = ['FC', 'H', 'LE', '-9999', '-9999', '-9999']
    FLXNT_qc_variable_flags = ['FC_SSITC_TEST', 'H_SSITC_TEST', 'LE_SSITC_TEST', '-9999', '-9999',
                               '-9999']

    FOF_qc_variable_position_in_9_digit_flag_number = [5, 4, 6, 6, 7, 8]  # code is e.g. 800000099, see full_output file
    FOF_qc_variable_position_in_5_digit_flag_number = [1, -9999, 2, 2, 3, 4]  # code is e.g. 89999
    FOF_qc_variable_position_in_2_digit_flag_number = [1, 1, 1, 1, 1, 1]  # code is e.g. 81
    FOF_qc_variable_max = [50, 800, 1000, 50, 0.8, 0.1]
    FOF_qc_variable_min = [-50, -200, -200, -20, -0.4, -0.05]
    # the column name of the measured AGC
    FOF_qc_variable_agc_available = [True, False, True, True, False,
                                     False]  # string that tags column containing AGC (window dirtiness)
    # FOF_qc_variable_agc_string_wd = ['window_dirtiness', -9999, 'window_dirtiness', 'window_dirtiness', -9999, -9999]  # string that tags column containing AGC (window dirtiness)
    # FOF_qc_variable_agc_string_sb = ['status_byte', -9999, 'status_byte', 'status_byte', -9999, -9999]  # string that tags column containing AGC (window dirtiness)

    site_list = ('CH-CHA', 'CH-DAV', 'CH-FRU', 'CH-LAE', 'CH-LAS', 'CH-OE2', 'CH-AWS', 'US-TWE', 'CH-INO')
    IRGA_list = ('Licor 7500 (open path)', 'Licor 7200 (enclosed path)')
    site_ustar_limit = [0.08, 0.2, 0.05, 0.3, 0.08, 0.08, 0.08, 0.08,
                        0.05]  # values for LAS, OE2 and TWE should be checked

    # LEGEND ------------------------------
    # FOF = full_output file
    # FLXNT = _fluxnet_ output file from EddyPro
    # -------------------------------------

    import matplotlib as mpl
    mpl.use('Agg')  # when calling savefig() instead of show() the pop-up window won’t appear
    import fnmatch
    import os
    import itertools
    import numpy as np
    import datetime as dt
    import matplotlib.pyplot as plt
    import pandas as pd
    import shutil
    from func import consider_flag_list_9_digits, consider_flag_list_5_digits, consider_flag_list_2_digits
    from os.path import basename
    import plot
    import gui
    # import seaborn as sns

    # sns.set_style("whitegrid")

    # NOTE: the new EP600 output contains the SSITC flag that will not be changed by this script
    # instead, for the FLUXNET output, fluxes that fail the raw data test will be set to -9999
    # (in accordance w/ the FLUXNET guidelines for data submission)
    # OLD:
    # we have different qc labels in the *_FLUXNET_eddy_* output file:
    # qc_label_in_FLXNET_file = ['QcFc_1_1_1', 'QcH_1_1_1', 'QcLE_1_1_1', 'QcE_1_1_1', 'QcFCH4_1_1_1', ' QcFN2O_1_1_1']
    # new columns for the overall QC flag (contains SSITC and the other checks)
    # qc_label_in_FLXNET_file = ['FC_QC_FINAL_FLAG_1_1_1', 'H_QC_FINAL_FLAG_1_1_1', 'LE_QC_FINAL_FLAG_1_1_1', 'E_QC_FINAL_FLAG_1_1_1', 'FCH4_QC_FINAL_FLAG_1_1_1', ' FN2O_QC_FINAL_FLAG_1_1_1']

    # GUI --------------------------------------------------------------------------
    [eddypro_qc, minimum_threshold, maximum_threshold, window_dirtiness, agc_setting,
     agc_threshold, ustar_hf, site_to_process, flag_list_9_digits, flag_list_5_digits,
     flag_list_2_digits, select_IRGA] \
        = gui.main_window(site_list=site_list, IRGA_list=IRGA_list)

    # class to write to console AND log file with print
    class Tee(object):
        def __init__(self, *files):
            self.files = files

        def write(self, obj):
            for ff in self.files:
                ff.write(obj)

        def flush(self):
            pass

    if site_to_process == 'Please select site ...':
        print("!ERROR: no site selected in settings")
        sys.exit("!ERROR: no site selected in settings")

    if select_IRGA == 'Please select used IRGA ...':
        print("!ERROR: no IRGA selected in settings")
        sys.exit("!ERROR: no IRGA selected in settings")

    root_folder = os.getcwd()

    allfiles = os.listdir(root_folder)
    print("Found files in " + root_folder + ":")
    print(allfiles)

    # --------------| SCAN EddyPro full_output FILE
    for FOF_found_file in allfiles:
        if fnmatch.fnmatch(FOF_found_file, '*.csv') and '_full_output_' in FOF_found_file:

            # construct now_string from current time
            now = dt.datetime.now()
            now_year = now.year
            now_month = now.month
            now_day = now.day
            now_hour = now.hour
            now_minute = now.minute
            now_second = now.second

            id_string = 'QC-{:4d}{:02d}{:02d}-{:02d}{:02d}{:02d}'.format(now_year, now_month, now_day, now_hour,
                                                                         now_minute,
                                                                         now_second)

            # now_string = str(now_year) + "-" + str(now_month).zfill(2) + "-" + str(now_day).zfill(2) + "T" + str(
            #     now_hour).zfill(2) + str(now_minute).zfill(2) + str(now_second).zfill(2)

            # setup folder structure for this file
            qc_folder = os.path.join(root_folder, "OUT_" + id_string)
            if os.path.exists(qc_folder):
                shutil.rmtree(qc_folder)
            if not os.path.exists(qc_folder):
                os.makedirs(qc_folder)

            # write all console output to logfile txt file
            logfile_out = os.path.join(qc_folder, "log")
            os.makedirs(logfile_out)
            logfile_out = os.path.join(logfile_out, "{}_log_FINAL_QC.txt".format(id_string))
            logfile_out = open(logfile_out, 'w')
            original = sys.stdout
            sys.stdout = Tee(sys.stdout, logfile_out)

            print("Quality control script version: {}".format(version))
            print("Quality control flux level-2 ID: {}".format(id_string))

            # SEARCH FLUXNET OUTPUT FILE *_fluxnet_*
            # e.g. eddypro_CH-LAE_fluxnet_2019-06-01T004323_adv.csv
            # look at the timestamp ID ([-21:-4] of filename), we use this id to match the correct FLUXNET file
            FOF_file_id = os.path.splitext(basename(FOF_found_file))[0]
            FOF_file_id = FOF_file_id[27:44]  # todo: find a more elegant way
            FLXNET_found = 0
            for FLXNET_found_file in allfiles:
                if fnmatch.fnmatch(FLXNET_found_file, '*.csv') and 'fluxnet_' in FLXNET_found_file:
                    print("Found FLUXNET output file " + FLXNET_found_file + ". Checking IDs...")
                    FLXNET_file_id = os.path.splitext(basename(FLXNET_found_file))[0]
                    FLXNET_file_id = FLXNET_file_id[23:40]
                    if FLXNET_file_id == FOF_file_id:
                        print("SUCCESS! File " + FLXNET_found_file + " has the same ID as " + FOF_found_file + ".")
                        FLXNET_found = 1
                        break
                    else:
                        print("Wrong ID, ignoring file " + FLXNET_found_file + ".")
            if FLXNET_found == 0:
                print("No FLUXNET output file matching ID " + FOF_file_id + " found.")
                FLXNET_found_file = '-9999'
                FLXNET_found = 0

            FOF_found_file_no_ending = "full_output"
            FLXNET_found_file_no_ending = "fluxnet"

            print("\n\n--------------------------------------------------")
            print("WORKING ON FILE:")
            print("FULL OUTPUT FILE: " + FOF_found_file)
            if FLXNET_found == 1:
                print("FLUXNET FILE: " + FLXNET_found_file)
            else:
                print("FLUXNET FILE: none found")
            print("--------------------------------------------------")

            FOF_plot_folder = os.path.join(qc_folder,
                                           "QC_plots_" + FOF_found_file_no_ending)  # make separate plot folder for each found file
            FOF_detail_folder = os.path.join(qc_folder,
                                             "QC_detail_" + FOF_found_file_no_ending)  # make separate plot folder for each found file
            os.makedirs(FOF_plot_folder)
            os.makedirs(FOF_detail_folder)

            # small function to read date column properly
            FOF_parse_date = lambda z: dt.datetime.strptime(z, '%Y-%m-%d %H:%M')

            # read data file, generates data frame (similar to R)
            if header_type == 0:
                FOF_contents = pd.read_csv(FOF_found_file, skiprows=[0, 2], parse_dates=[['date', 'time']],
                                           keep_date_col=True, index_col=0, date_parser=FOF_parse_date,
                                           encoding='utf-8')
                # read start of data file to get FOF_units (in 3rd row)
                FOF_units = pd.read_csv(FOF_found_file, skiprows=[0, 1], nrows=1, mangle_dupe_cols=True,
                                        encoding='utf-8')
                FOF_original = FOF_units  # we need all columns for final output of full_output file
                FOF_units = FOF_units.columns[1:]  # skip the first column (filename)

            else:
                # assumes that there is an empty line after the 2-row header no longer necessary
                # previous versions of pandas had this empty line in the file after files were merged
                FOF_contents = pd.read_csv(FOF_found_file, parse_dates=[['date', 'time']], header=0, index_col=0,
                                           keep_date_col=True, encoding='utf-8', skiprows=[1])

                FOF_units = pd.read_csv(FOF_found_file, skiprows=[0], nrows=1, mangle_dupe_cols=True,
                                        encoding='utf-8')
                FOF_original = FOF_units  # we need all columns for final output of full_output file
                FOF_units = FOF_units.columns  # skip the first column (filename)

            FOF_columns_names_original = FOF_contents.columns  # we need all columns for final output of full_output file
            FOF_columns_names = FOF_contents.columns  # skip the first column (filename)
            # FOF_columns_names = FOF_contents.columns[1:]  # skip the first column (filename) todo this was a bug for header_type=1, resulted in wrong plot units
            FOF_columns_count = len(FOF_contents.columns) - 1

            # reset!
            FOF_measured_variable_list = []
            FOF_measured_variable_position_in_9_digit_flag_number = []  # code is e.g. 800000099, see full_output file
            FOF_measured_variable_position_in_5_digit_flag_number = []  # code is e.g. 89999
            FOF_measured_variable_position_in_2_digit_flag_number = []  # code is e.g. 81
            FOF_measured_variable_max = []
            FOF_measured_variable_min = []
            FOF_measured_AGC_variable_available = []
            FOF_measured_variable_status_byte_col = []

            # search available variables
            "... searching for measured variables in full_output file"
            for x in range(0, len(FOF_qc_variable_list)):
                if FOF_qc_variable_list[x] in FOF_contents.columns:
                    print("\n...... [+] found variable: " + FOF_qc_variable_list[x])
                    FOF_measured_variable_list.append(FOF_qc_variable_list[x])

                    FOF_measured_variable_position_in_9_digit_flag_number.append(
                        FOF_qc_variable_position_in_9_digit_flag_number[x])
                    FOF_measured_variable_position_in_5_digit_flag_number.append(
                        FOF_qc_variable_position_in_5_digit_flag_number[x])
                    FOF_measured_variable_position_in_2_digit_flag_number.append(
                        FOF_qc_variable_position_in_2_digit_flag_number[x])
                    FOF_measured_variable_max.append(FOF_qc_variable_max[x])
                    FOF_measured_variable_min.append(FOF_qc_variable_min[x])
                    FOF_measured_AGC_variable_available.append(FOF_qc_variable_agc_available[x])
                    # FOF_measured_variable_status_byte_col.append(FOF_qc_variable_agc_string_sb[x])

                    print("......... with these settings:")
                    print("............ MIN: " + str(FOF_qc_variable_min[x]))
                    print("............ MAX: " + str(FOF_qc_variable_max[x]))
                    print("............ FOF_measured_variable_position_in_9_digit_flag_number: " + str(
                        FOF_qc_variable_position_in_9_digit_flag_number[x]))
                    print("............ FOF_measured_variable_position_in_5_digit_flag_number: " + str(
                        FOF_qc_variable_position_in_5_digit_flag_number[x]))
                    print("............ FOF_measured_variable_position_in_2_digit_flag_number: " + str(
                        FOF_qc_variable_position_in_2_digit_flag_number[x]))
                    print("............ FOF_measured_AGC_variable_available: " + str(FOF_qc_variable_agc_available[x]))
                    # print("............ OR: column for FOF_measured_variable_status_byte_col: " + str(FOF_qc_variable_agc_string_sb[x]))

            print("\n... these variables were considered during search:")
            print(*FOF_qc_variable_list, sep='\n')
            print("\n\n")

            # also read FLUXNET file (if found)
            if FLXNET_found == 1:
                # in recent versions of pandas: no more empty lines in merged files
                # FLXNET_skiprows_contents = [1]
                # FLXNET_skiprows_units = [0]

                # this loses the original timestamp
                FLXNET_parse_date = lambda z: dt.datetime.strptime(z, FLXNET_file_dateformat)
                FLXNET_contents = pd.read_csv(FLXNET_found_file, skiprows=None,
                                              keep_date_col=True, index_col=fxn_index_col,
                                              date_parser=FLXNET_parse_date)
                # FLXNET_units = pd.read_csv(FLXNET_found_file, skiprows=FLXNET_skiprows_units, nrows=1, mangle_dupe_cols=True)
                # FLXNET_units = FLXNET_units.columns[
                #             1:]  # skip the first column (yyyymmddHHMM... we generate our own timestamp column name incl units)
                FLXNET_stripped_column_names = FLXNET_contents.rename(
                    columns=lambda x: x.strip())  # remove whitespace from column headers
                FLXNET_contents.columns = FLXNET_stripped_column_names.columns

                # the FLUXNET file contains date gaps, a difference to the full_output file, therefore:
                FLXNET_contents = fill_date_gaps(FLXNET_contents, '30T')

            print("number of columns: " + str(FOF_columns_count))
            for x in range(0, FOF_columns_count):
                # read columns names from file, count columns in file
                print("#" + str(x) + " " + str(FOF_columns_names[x]) + " | FOF_units --> " + FOF_units[x])

            # construct DataFrame that will contain all variables original (no QC) and controlled (with QC)
            detail_DataFrame = pd.DataFrame(index=FOF_contents.index)  # d = detail

            # CYCLE through variables that have to be QC'd
            for FOF_found_measured_variable in FOF_measured_variable_list:
                ustar_data = FOF_contents['u*']  # for ustar filtering
                measured_variable_index_in_list = FOF_measured_variable_list.index(FOF_found_measured_variable)
                FOF_y = FOF_contents[FOF_found_measured_variable]  # get variable data via variable name
                FOF_y_units = FOF_columns_names.get_loc(
                    FOF_found_measured_variable)  # find position of current variable
                FOF_y_units = FOF_units[FOF_y_units]  # use this position to get the respective FOF_units of variable

                # also get the FOF_found_measured_variable from the FLUXNET file
                if FLXNET_found == 1:
                    FLXNET_index = FOF_qc_variable_list.index(FOF_found_measured_variable)
                    if FLXNT_qc_variable_list[
                        FLXNET_index] != '-9999':  # this means that the var is not part of the FLUXNET output
                        FLXNET_y = FLXNET_contents[FLXNT_qc_variable_list[FLXNET_index]]
                else:
                    FLXNET_index = -9999

                # sometimes EddyPro outputs 'Infinity' in the results, which is a problem because it is not a number
                # in that case, the whole data Series is regarded as string, also the numbers in the Series
                # to account for this error we search 'Infinity' in the result column and convert it to a missing value
                # after replacing all string values in the Series we can finally convert it to NUMERIC
                FOF_y = FOF_y.replace('Infinity', '-9999')  # replace 'Infinity' string (if found...)
                FOF_y = FOF_y.replace('-Infinity', '-9999')  # replace 'Infinity' string (if found...)
                FOF_y = FOF_y.replace('#NAME?',
                                      '-9999')  # happens when a csv with "-Infinity" is opened in Excel and saved again, "-Infinity" becomes "#NAME?"
                FOF_y = FOF_y.replace('NaN', '-9999')  # should not be necessary...
                FOF_y = FOF_y.replace('#N/A', '-9999')  # should not be necessary...
                FOF_y = FOF_y.replace('#NV', '-9999')  # should not be necessary...
                FOF_y = FOF_y.astype(float)  # convert Series to NUMERIC

                detail_DataFrame[
                    FOF_found_measured_variable] = FOF_y  # store variable without QC to pandas DataFrame for later detail output
                # plot_DataFrame = pd.DataFrame(index=FOF_y.index, columns=flag_list_of_all_selected)  # make an empty DataFrame
                # with flag names as header (index is the same as always)
                # make an empty DataFrame with variable name as header (index is the same as always)
                plot_DataFrame = pd.DataFrame(index=FOF_y.index, columns=[FOF_found_measured_variable])

                # now lets start filling values in our empty DataFrame
                # first we have the complete unchanged FOF_found_measured_variable time series as read from the file
                plot_DataFrame[FOF_found_measured_variable] = FOF_y
                # let's also store this time series in a new variable
                # we need this variable to calculate how many values we lose because of a single quality criterion
                # (note: qc overlap, i.e. we can lose a data point because e.g. 5 criteria flag the point as bad)
                full_output_y_original = plot_DataFrame[FOF_found_measured_variable]

                qc_string = 'qc_' + FOF_found_measured_variable  # this the EddyPro prefix for quality controlled variables

                if qc_string in FOF_columns_names:

                    print("\n")
                    print("---------------------------------------")
                    print(FOF_found_measured_variable + " VALUES:")
                    print("---------------------------------------")

                    plot_scatter_string = []  # will contain all variable names to be plotted
                    plot_legend_string = []  # initiate legend for plotting

                    # BEFORE QC
                    print("* before quality control: " + str(len(FOF_y)) + "\n")

                    # WITHOUT MISSING VALUES
                    bad_data_positions = full_output_y_original == -9999  # missing values, qc is boolean and results in TRUE if it finds -9999 at the position, FALSE means we have a non-missing value
                    bad_data_values = full_output_y_original[
                        bad_data_positions]  # compared to original FOF_y; before we remove them, store bad data points in variable for later use (plotting)
                    plot_DataFrame[
                        'missing_values'] = bad_data_values  # add data points that are lost because of eddypro_qc to the DataFrame for later output
                    plot_DataFrame[
                        'missing_flag'] = bad_data_positions  # add data points that are lost because of eddypro_qc to the DataFrame for later output

                    # Let's setup a new empty DataFrame that will contain only the final QC flag
                    finalflag_DataFrame = pd.DataFrame(index=bad_data_positions.index, columns=["QC_FINAL_FLAG"])
                    finalflag = finalflag_DataFrame['QC_FINAL_FLAG']  # extract the column, becomes Series
                    finalflag.fillna(0, inplace=True)  # by default all values are OK, i.e. flag = 0, however...

                    finalflag[
                        bad_data_positions] = -9999  # 2015-05-06 ... let missing be missing, we still need -9999 for all originally missing values (because of the optional merging later on) # flag bad data as "do not use!". i.e. flag = 2

                    FOF_y[
                        bad_data_positions] = -9999  # set missing values to -9999, in this case of course nothing changes, done for consistency
                    y_length_without_missing_values = len(FOF_y[FOF_y != -9999])  # length of FOF_y without -9999
                    print("* without missing values: " + str(y_length_without_missing_values) + "(100%)\n")

                    if y_length_without_missing_values > 0:

                        # EDDYPRO QC FLAG (SSITC tests)  # relevant for FLUXNET file: no, already available in output flag
                        if eddypro_qc == 1:
                            bad_data_positions = (FOF_contents[qc_string] > 1) & (FOF_contents[qc_string] != -9999) & (
                                    full_output_y_original != -9999)  # find positions of bad data, TRUE means all positions where the qc_ flag > 1, i.e. bad data; ignores all already missing data (-9999)

                            # in case of this flag, we have a soft flag which is indicated by the number 1; this is more or less good data
                            bad_data_positions_soft_flag = (FOF_contents[qc_string] == 1) & (
                                    FOF_contents[qc_string] != -9999) & (
                                                                   full_output_y_original != -9999)  # find positions of bad data, TRUE means all positions where the qc_ flag > 1, i.e. bad data; ignores all already missing data (-9999)
                            bad_data_values = full_output_y_original[
                                bad_data_positions]  # compared to original FOF_y; before we remove them, store bad data points in variable for later use (plotting)
                            criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                                                      2)  # percentage of data lost b/c of this criterion
                            plot_DataFrame[
                                'eddypro_qc_values'] = bad_data_values  # add data points that are lost because of eddypro_qc to the DataFrame for later output
                            plot_DataFrame[
                                'eddypro_qc_flag'] = bad_data_positions  # add data points that are lost because of eddypro_qc to the DataFrame for later output

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2
                            finalflag[
                                bad_data_positions_soft_flag] += 1  # flag bad data as "not perfect but OK data". i.e. add 1

                            FOF_y[
                                bad_data_positions] = -9999  # * now remove bad data from FOF_y time series, FOF_y is passed on, set bad data to missing values
                            remaining_data_points = len(
                                FOF_y[FOF_y != -9999])  # counts remaining data points in time series that are not -9999
                            print("* after EddyPro qc flag (SSITC tests): " + str(remaining_data_points) + "(" + str(
                                np.round((remaining_data_points / y_length_without_missing_values) * 100,
                                         2)) + "% remain)")
                            print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")
                            # *note on full_output_y_original: because we search bad_data_values in the full_output_y_original series but set those
                            # bad values to -9999 in the FOF_y series that is passed on, it is possible that a value was
                            # already flagged by a previous flag as bad (qc flags overlap),
                            # i.e. in some cases we set -9999 to -9999 (when a previously bad data point is flagged again)
                            # but of course this has no serious consequences

                        # MINIMUM THRESHOLD  # hard flag; relevant for FLUXNET file: no
                        if minimum_threshold == 1:
                            # note on bad_data_positions: we flag only positions that are not missing, i.e. -9999 is excluded from this analysis because it's not a value but missing data
                            bad_data_positions = (full_output_y_original < FOF_measured_variable_min[
                                measured_variable_index_in_list]) & (
                                                         full_output_y_original != -9999)  # yields TRUE for all data points below minimum, ignores already missing data (-9999)
                            bad_data_values = full_output_y_original[
                                bad_data_positions]  # these are the values that are lost only b/c of this criterion
                            criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                                                      2)  # percentage of data lost b/c of this criterion
                            plot_DataFrame['minimum_threshold_values'] = bad_data_values
                            plot_DataFrame['minimum_threshold_flag'] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            FOF_y[bad_data_positions] = -9999
                            remaining_data_points = len(FOF_y[FOF_y != -9999])
                            print("* after removing values below " + str(
                                FOF_measured_variable_min[measured_variable_index_in_list]) + ": " + str(
                                remaining_data_points) + "(" + str(
                                np.round((remaining_data_points / y_length_without_missing_values) * 100,
                                         2)) + "% remain)")
                            print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

                            plot_legend_string.append(FOF_measured_variable_list[measured_variable_index_in_list])

                        # MAXIMUM THRESHOLD  # hard flag; relevant for FLUXNET file: no
                        if maximum_threshold == 1:
                            bad_data_positions = (full_output_y_original > FOF_measured_variable_max[
                                measured_variable_index_in_list]) & (
                                                         full_output_y_original != -9999)  # yields TRUE for all data points above maximum; ignores already missing data (-9999)
                            bad_data_values = full_output_y_original[bad_data_positions]
                            criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                                                      2)  # percentage of data lost b/c of this criterion
                            plot_DataFrame['maximum_threshold_values'] = bad_data_values
                            plot_DataFrame['maximum_threshold_flag'] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            FOF_y[bad_data_positions] = -9999
                            remaining_data_points = len(FOF_y[FOF_y != -9999])
                            print("* after removing values above " + str(
                                FOF_measured_variable_max[measured_variable_index_in_list]) + ": " + str(
                                remaining_data_points) + "(" + str(
                                np.round((remaining_data_points / y_length_without_missing_values) * 100,
                                         2)) + "% remain)")
                            print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

                            plot_legend_string.append(FOF_measured_variable_list[measured_variable_index_in_list])

                        # WINDOW DIRTINESS (AGC)  # hard flag; relevant for FLUXNET file: yes, values set to -9999
                        if window_dirtiness == 1 and FOF_measured_AGC_variable_available[
                            measured_variable_index_in_list]:

                            print("Searching for AGC / SIGNAL STRENGTH variables...")
                            print("  Selected IRGA: {}".format(select_IRGA))
                            print("  Flux variable: {}".format(FOF_found_measured_variable))
                            # search for columns containing AGC data named 'windows_dirtiness'
                            agc_cols = []
                            for col in FOF_contents.columns:
                                if 'status_byte' in col:
                                    print("  Found " + col)
                                    agc_cols.append(col)
                                elif 'window_dirtiness' in col:
                                    print("  Found " + col)
                                    agc_cols.append(col)
                                elif 'signal_strength' in col:
                                    print("  Found " + col)
                                    agc_cols.append(col)

                            print("  Found the following variables: {}".format(agc_cols))
                            print('  Trying to match {} with {}'.format(select_IRGA, agc_cols))

                            # if there is more than 1 column let's check which one is the right one
                            if len(agc_cols) > 1:
                                for x in range(0, len(agc_cols)):  # let's look at all found AGC columns...
                                    if select_IRGA == 'Licor 7500 (open path)':
                                        if '75' in agc_cols[x]:
                                            measured_variable_agc = agc_cols[x]
                                            print('  For {} the relevant AGC variable is: {}'.format(select_IRGA,
                                                                                                     measured_variable_agc))
                                    elif select_IRGA == 'Licor 7200 (enclosed path)':
                                        if '72' in agc_cols[x]:
                                            measured_variable_agc = agc_cols[x]
                                            print('  For {} the relevant AGC variable is: {}'.format(select_IRGA,
                                                                                                     measured_variable_agc))

                                if not measured_variable_agc:
                                    print('\n\n*** NO AGC VARIABLE FOUND! ***\n\n')

                            else:
                                measured_variable_agc = agc_cols[0]

                            agc = FOF_contents[measured_variable_agc]
                            # agc = FOF_contents[measured_variable_agc[measured_variable_index_in_list]]

                            if agc_setting == 'flag AGC values above':
                                bad_data_positions = (agc > agc_threshold) & (agc != -9999) & (
                                        full_output_y_original != -9999)  # ignore already missing data
                                print("Flagging all AGC values ABOVE " + str(agc_threshold))
                            elif agc_setting == 'flag AGC values below':  # e.g. Li7200 after software update --> 50 = bad signal strength
                                bad_data_positions = (agc < agc_threshold) & (agc != -9999) & (
                                        full_output_y_original != -9999)
                                print("Flagging all AGC values BELOW " + str(agc_threshold))

                            bad_data_values = full_output_y_original[bad_data_positions]
                            criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                                                      2)  # percentage of data lost b/c of this criterion
                            plot_DataFrame['AGC_values'] = bad_data_values
                            plot_DataFrame['AGC_flag'] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            FOF_y[bad_data_positions] = -9999

                            if FLXNET_found == 1:
                                if FLXNT_qc_variable_list[
                                    FLXNET_index] != '-9999':  # set bad values to -9999 in FLXNET, but only if var is part of FLUXNET output file
                                    FLXNET_y[bad_data_positions] = -9999

                            remaining_data_points = len(
                                FOF_y[FOF_y != -9999])  # counts remaining data points in time series that are not -9999
                            print("* after removing AGC issues: " + str(remaining_data_points) + "(" + str(
                                np.round((remaining_data_points / y_length_without_missing_values) * 100,
                                         2)) + "% remain)")
                            print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

                            plot_legend_string.append(FOF_measured_variable_list[measured_variable_index_in_list])
                        else:
                            print("** No AGC value available for " + str(FOF_qc_variable_agc_available[
                                                                             measured_variable_index_in_list]) + ". Skipping this check.\n")

                        # USTAR FILTER  # relevant for FLUXNET file: no, has separate output column in file
                        if ustar_hf == 1:
                            measured_variable_ustar_index_in_list = site_list.index(
                                site_to_process)  # gives index number of site name in site list ...
                            ustar_limit_this_site = site_ustar_limit[
                                measured_variable_ustar_index_in_list]  # ... uses that index number to get u* limit for the site
                            print("using u* limit for site " + site_to_process)
                            bad_data_positions = (ustar_data < ustar_limit_this_site) & (
                                    full_output_y_original != -9999)  # yields TRUE for all data points below minimum, ignores already missing data (-9999)
                            bad_data_values = full_output_y_original[
                                bad_data_positions]  # these are the values that are lost only b/c of this criterion
                            criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                                                      2)  # percentage of data lost b/c of this criterion
                            plot_DataFrame['ustar_values'] = bad_data_values
                            plot_DataFrame['ustar_flag'] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            FOF_y[bad_data_positions] = -9999
                            remaining_data_points = len(FOF_y[FOF_y != -9999])
                            print("* after removing values where u* below limit of  " + str(
                                ustar_limit_this_site) + ": " + str(
                                remaining_data_points) + "(" + str(
                                np.round((remaining_data_points / y_length_without_missing_values) * 100,
                                         2)) + "% remain)")
                            print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

                            plot_legend_string.append(FOF_measured_variable_list[measured_variable_index_in_list])

                        # 9-DIGIT FLAG CODE, raw data screening  # relevant for FLUXNET file: yes, values set to -9999
                        for x in range(0, len(flag_list_9_digits)):  # cycle through all flags in full_output file
                            results = consider_flag_list_9_digits \
                                (flag_list_9_digits[x], FOF_contents,
                                 FOF_measured_variable_position_in_9_digit_flag_number, measured_variable_index_in_list,
                                 FOF_y, y_length_without_missing_values, plot_legend_string, full_output_y_original)
                            FOF_y = results[0]
                            bad_data_values = results[1]
                            bad_data_positions = results[2]

                            if FLXNET_found == 1:
                                if FLXNT_qc_variable_list[
                                    FLXNET_index] != '-9999':  # set bad values to -9999 in FLUXNET, but only if var is part of FLUXNET output file
                                    FLXNET_y[bad_data_positions] = -9999

                            plot_DataFrame[flag_list_9_digits[x] + "_values"] = bad_data_values
                            plot_DataFrame[flag_list_9_digits[x] + "_flag"] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            plot_legend_string = results[3]
                            # output of this function: [0] = FOF_y (flag-filtered), bad_data (collects all bad data points for later plotting)
                            # the flag-filtered FOF_y of each run is handed over to the next run

                        # 5-DIGIT FLAG CODE, timelag flag  # relevant for FLUXNET file: no
                        bad_data_values = []
                        for x in range(0, len(flag_list_5_digits)):  # cycle through all flags in full_output file
                            results = consider_flag_list_5_digits(flag_list_5_digits[x], FOF_contents,
                                                                  FOF_measured_variable_position_in_5_digit_flag_number,
                                                                  measured_variable_index_in_list, FOF_y,
                                                                  y_length_without_missing_values, plot_legend_string,
                                                                  full_output_y_original)
                            FOF_y = results[0]
                            bad_data_values = results[1]
                            bad_data_positions = results[2]
                            plot_DataFrame[flag_list_5_digits[x] + "_values"] = bad_data_values
                            plot_DataFrame[flag_list_5_digits[x] + "_flag"] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            plot_legend_string = results[3]
                            # output of this function: [0] = FOF_y (flag-filtered), bad_data (collects all bad data points for later plotting)
                            # the flag-filtered FOF_y of each run is handed over to the next run

                        # 2-DIGIT FLAG CODE, attack angle & non-steady wind  # relevant for FLUXNET file: maybe at some point ...
                        bad_data_values = []
                        for x in range(0, len(flag_list_2_digits)):  # cycle through all flags in full_output file
                            results = consider_flag_list_2_digits(flag_list_2_digits[x], FOF_contents,
                                                                  FOF_measured_variable_position_in_2_digit_flag_number,
                                                                  measured_variable_index_in_list, FOF_y,
                                                                  y_length_without_missing_values, plot_legend_string,
                                                                  full_output_y_original)
                            FOF_y = results[0]
                            bad_data_values = results[1]
                            bad_data_positions = results[2]
                            plot_DataFrame[flag_list_2_digits[x] + "_values"] = bad_data_values
                            plot_DataFrame[flag_list_2_digits[x] + "_flag"] = bad_data_positions

                            finalflag[bad_data_positions] += 2  # flag bad data as "do not use!". i.e. add 2

                            if FLXNET_found == 1:
                                if FLXNT_qc_variable_list[
                                    FLXNET_index] != '-9999':  # set bad values to -9999 in FLUXNET, but only if var is part of FLUXNET output file
                                    FLXNET_y[bad_data_positions] = -9999

                            plot_legend_string = results[3]
                            # output of this function: [0] = FOF_y (flag-filtered), bad_data (collects all bad data points for later plotting)
                            # the flag-filtered FOF_y of each run is handed over to the next run
                    else:
                        print("ATTENTION! Length of " + FOF_found_measured_variable + " is zero! No data available!")

                    # finally we add the _QC variable and the finalflag to our plot DataFrame
                    # we use .insert to put it into column 1 (i.e. the second) so it is right next to the original time series
                    plot_DataFrame.insert(1, FOF_found_measured_variable + "_QC_FINAL_FLAG", FOF_y)

                    # we also add the finalflag Series, right next to the QC data values, i.e. third column
                    # but before we do that, we set all values > 2 to 2, so we have the 0-1-2 flag system
                    finalflag[finalflag >= 2] = 2

                    # As a security measure
                    # There is the theoretical possibility that under certain
                    # circumstances we get -9997 or -9995 etc...:
                    # The reason for this is that we build the final flag by adding the number 2
                    # to our finalflag number. If data were originally missing but for some reason
                    # one of the parameters for the QC check is available and yields 2, the script would
                    # calculate the following: -9999 + 2 = -9997
                    # But for clean processing we strictly need the -9999 as missing value.
                    finalflag[finalflag < 0] = -9999  # 2015-05-06

                    plot_DataFrame.insert(2, "QC_FINAL_FLAG", finalflag)

                    # PLOTTING
                    colors = itertools.cycle(['black', '#1f78b4', '#33a02c', '#e31a1c',
                                              '#ff7f00', '#cab2d6', '#6a3d9a', '#b15928', '#a6cee3', '#b2df8a',
                                              '#fb9a99', '#fdbf6f', 'red'])  # my own color cycler for the scatter plot

                    # figure.patch.set_facecolor('#CCCCCC')
                    # Two subplots, unpack the axes array immediately

                    fig = plt.figure(1)
                    gridspec1 = gridspec.GridSpec(2, 1)
                    gridspec1.update(wspace=0.4, hspace=0.15)  # set the spacing between axes.
                    ax1 = fig.add_subplot(gridspec1[0, 0])
                    ax2 = fig.add_subplot(gridspec1[1, 0])

                    # plt.scatter(FOF_y.index, plot_DataFrame[FOF_found_measured_variable + "_QC"], color='black', s=4)  # _QC variable, the _QC variable is plotted extra in black
                    legend_string = []  # reset legend text string

                    for x in range(1, len(plot_DataFrame.columns)):  # start at index 1, ignore original variable;
                        if '_QC' in plot_DataFrame.columns[x] or '_values' in plot_DataFrame.columns[
                            x]:  # plot only _QC values and columns that contain values, ignore the _flag columns
                            data = plot_DataFrame[plot_DataFrame.columns[x]]
                            if x == 1:
                                ax1.plot_date(data.index, data, color=next(colors), alpha=0.7, ms=0.5, lw=1)
                                # ax1.scatter(data.index, data, color=next(colors), s=1.5, alpha=0.7)

                                data_DA = data.replace(-9999, np.nan).resample('D').mean()
                                data_SD = data.replace(-9999, np.nan).resample('D').std()
                                ax2.plot_date(data_DA.index, data_DA, color='black', alpha=1, label='daily average',
                                              ms=0.5, lw=1)
                                # ax2.scatter(data_DA.index, data_DA, color='black', s=3, alpha=1, label='daily average')
                                ax2.set_ylim(data_DA.min(), data_DA.max())
                                ax2.errorbar(data_DA.index, data_DA, alpha=0.2,
                                             yerr=data_SD, capsize=0, ls='none', color='black', elinewidth=1, label='')
                                ax2.legend()

                            else:
                                ax1.plot_date(data.index, data, color=next(colors), alpha=0.6, ms=0.5, lw=1)
                                # ax1.scatter(data.index, data, color=next(colors), s=1, alpha=0.6)
                            # add name of variable to legend text
                            legend_string.append(data.name)

                    leg = ax1.legend(legend_string, scatterpoints=8, loc='upper right', ncol=3, fontsize=8,
                                     fancybox=True)  # again, start at index 1, do not plot original variable

                    ax1.set_ylim(FOF_measured_variable_min[measured_variable_index_in_list] * 1.2,
                                 FOF_measured_variable_max[measured_variable_index_in_list] * 1.2)
                    leg.get_frame().set_alpha(0.8)
                    leg.get_frame().set_facecolor('white')

                    plot_name = os.path.join(FOF_plot_folder, FOF_found_measured_variable + "_QC.png")
                    plt.savefig(plot_name, dpi=200)
                    plt.close()

                    # PLOT DIURNAL CYCLES OF FINAL QC DATA
                    for x in range(1, len(plot_DataFrame.columns)):  # start at index 1, ignore original variable;
                        if '_QC' in plot_DataFrame.columns[x]:  # this is the final QC data
                            plot.plot_diurnal_cycles(FOF_found_measured_variable,
                                                     plot_DataFrame[plot_DataFrame.columns[x]], FOF_y_units,
                                                     FOF_plot_folder)

                    # [DETAIL] also add finalflag to the detail DataFrames
                    detail_DataFrame[FOF_found_measured_variable + "_QC_FINAL_FLAG"] = finalflag

                    # [FULL OUTPUT] we also want to output the full_output file, but with the new FINAL QC FLAG
                    FOF_contents[qc_string] = finalflag  # overwrite EddyPro qc_ with our new final QC
                    FOF_contents.rename(columns={qc_string: FOF_found_measured_variable + "_QC_FINAL_FLAG"},
                                        inplace=True)

                    # add QC'd variable values to output DataFrame that contains all variables
                    detail_DataFrame[FOF_found_measured_variable + "_QC_FINAL_FLAG"] = FOF_y

                    plot_DataFrame.to_csv(os.path.join(FOF_detail_folder,
                                                       FOF_found_file_no_ending + "_" + FOF_found_measured_variable + "_QC_FINAL_FLAG_detail_" + id_string + ".csv"),
                                          header=True)

            # now we need to output the full_output file with the original double header
            # we also take the liberty to insert a proper timestamp in column 0 (the first one)
            header_a = [str(x, ) for x in FOF_columns_names_original]  # generates list of column names for MultiIndex
            header_b = [str(x, ) for x in FOF_original]  # generates list of FOF_units for MultiIndex
            header_a.insert(0,
                            'TIMESTAMP')  # insert header for timestamp that is converted from type pandas index to a data column
            header_b.insert(0, '[yyyy-mm-dd HH:MM]')  # we have a double header
            headers_ab = pd.MultiIndex.from_arrays([header_a, header_b])  # generates our new double header
            FOF_contents.insert(0, FOF_contents.index.name, FOF_contents.index)
            FOF_contents = pd.DataFrame(FOF_contents.values, columns=headers_ab)  # merge

            # FOF_contents = FOF_contents.set_index(['date_time']) todo? (this is already done in EddyPro for the full_output file)
            # if fill_gaps == 1:
            #     FOF_contents = build_continuous_timestamp(FOF_contents)

            # we can now output FOF_contents directly:
            fo_final_qc_filename = os.path.join(qc_folder,
                                                FOF_found_file_no_ending + "_QC_FINAL_FLAG_" + id_string + ".csv")
            FOF_contents.to_csv(fo_final_qc_filename, header=True, index=False, encoding='utf-8')
            # FOF_contents.to_csv(fo_final_qc_filename, header=True, index=False, encoding='utf-8')
            print("\n--------------------------------------------------")
            print("--------------------------------------------------")
            print("Checking if length of output files are the same.")
            print("length of " + fo_final_qc_filename + " = " + str(len(FOF_contents)))

            # we also output a file that contains only the fluxes:
            d_fluxes_only_filename = FOF_found_file_no_ending + "_QC_FINAL_FLAG_" + id_string + ".csv"
            d_fluxes_only_filename = d_fluxes_only_filename.replace('full_output', 'fluxes_only')
            d_fluxes_only_filename = os.path.join(FOF_detail_folder, d_fluxes_only_filename)
            detail_DataFrame.to_csv(d_fluxes_only_filename, header=True)
            print("length of " + d_fluxes_only_filename + " = " + str(len(detail_DataFrame)))

            # now for the FLUXNET file
            # insert a proper timestamp in column 0 (the first one)
            if FLXNET_found == 1:

                print('FLUXNET File: setting SSITC flag to -9999 for missing values')
                for index, element in enumerate(FLXNT_qc_variable_list):
                    if FLXNT_qc_variable_flags[index] != '-9999':
                        missing_values_pos = FLXNET_contents[element] == -9999
                        FLXNET_contents[FLXNT_qc_variable_flags[index]][missing_values_pos] = -9999

                FLXNET_contents.insert(0, 'time', FLXNET_contents.index.time)  # insert time column, e.g. 13:30
                FLXNET_contents.insert(0, 'date', FLXNET_contents.index.date)  # insert date column, e.g. 2015-09-22

                # header_a = [str(x, ) for x in FLXNET_contents.columns]  # generates list of column names for MultiIndex
                # header_b = [str(x, ) for x in FLXNET_units]  # generates list of FLXNET_units for MultiIndex
                # header_b.insert(0, '[HH:MM]')  # we have a double header
                # header_b.insert(0, '[yyyy-mm-dd]')
                # header_b = [w.replace('[', '') for w in header_b]
                # header_b = [w.replace(']', '') for w in header_b]
                # headers_ab = pd.MultiIndex.from_arrays([header_a, header_b])  # generates our new double header

                # FLXNET_contents = pd.DataFrame(FLXNET_contents.values, columns=headers_ab)  # merge

                # we can now output FOF_contents directly:
                FLXNET_final_qc_filename = os.path.join(qc_folder,
                                                        FLXNET_found_file_no_ending + " " + id_string + ".csv")
                FLXNET_contents.to_csv(FLXNET_final_qc_filename, header=True, index=False, encoding='utf-8')
                print("length of " + FLXNET_final_qc_filename + " = " + str(len(FLXNET_contents)))

    print("\nI finished your work for you.")
    logfile_out.close()


def build_continuous_timestamp(dataframe):
    # FILLING TIMESTAMP GAPS WITH -9999 todo
    print("\nGenerating continuous timestamp from " + str(dataframe.index[0]) + " until " + str(dataframe.index[-1]))
    # generate continuous date range and re-index data
    filled_date_range = pd.date_range(dataframe.index[0], dataframe.index[-1], freq='30T')
    dataframe = dataframe.reindex(filled_date_range, fill_value=-9999)  # apply new continuous index to data

    return dataframe


def fill_date_gaps(dataframe, freq):
    print("--> Generating continuous datetime timestamp from " + str(dataframe.index[0]) + " until " + str(
        dataframe.index[-1]) + " ...")
    filled_date_range = pd.date_range(dataframe.index[0], dataframe.index[-1],
                                      freq=freq)  # generate continuous date range and re-index data
    dataframe = dataframe.reindex(filled_date_range, fill_value=-9999)  # apply new continuous index to data
    return dataframe


def consider_flag_list_9_digits(flag_name, contents, measured_variable_position_in_flag_code,
                                measured_variable_index_in_list, y, y_length_without_missing_values, plot_legend_string,
                                y_original):
    flag_code = contents[flag_name]  # contains flag_code code (and pandas index!)
    flag_code[flag_code == -9999] = 999999999  # set missing data to 9-digit eddypro flag_code code
    flag_code_str = flag_code.astype(str)  # Series; convert flag_code code to str
    single_flag = []  # list; empty; will only contain the flag for the compound

    for x in range(0, len(flag_code_str)):  # construct complete time series of the single flag; x is the row number
        single_flag.append(flag_code_str[x][measured_variable_position_in_flag_code[
            measured_variable_index_in_list]])  # co2 spike flag_code is in position 5 (i.e. index 4)
        # print(flag_code_str[x][:])

    df = pd.Series(single_flag, index=flag_code_str.index).astype(
        int)  # convert list --> pandas Series, index is the original timestamp, Series is also converted to integer
    bad_data_positions = (df == 1) & (
            y_original != -9999)  # yields TRUE if 1, 1 = bad data; flag can be 9 if test in EddyPro was not selected, ignores data that was already missing flag can be 9 if test in EddyPro was not selected
    bad_data_values = y_original[bad_data_positions]
    criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                              2)  # percentage of data lost b/c of this criterion
    y[bad_data_positions] = -9999
    remaining_data_points = len(y[y != -9999])  # counts remaining data points in time series that are not -9999
    print("* after considering " + flag_name + ": " + str(remaining_data_points) + "(" + str(
        np.round((remaining_data_points / y_length_without_missing_values) * 100, 2)) + "% remain)")
    print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

    plot_legend_string.append(flag_code)

    return y, bad_data_values, bad_data_positions, plot_legend_string


def consider_flag_list_5_digits(flag_name, contents, measured_variable_position_in_flag_code,
                                measured_variable_index_in_list, y, y_length_without_missing_values, plot_legend_string,
                                y_original):
    flag_code = contents[flag_name]  # contains flag_code code (and pandas index!)
    flag_code[flag_code == -9999] = 99999  # set missing data to 5-digit eddypro flag_code code
    flag_code_str = flag_code.astype(str)  # Series; convert flag_code code to str
    single_flag = []  # list; empty; will only contain the flag for the compound

    for x in range(0, len(flag_code_str)):  # construct complete time series of the single flag; x is the row number
        single_flag.append(flag_code_str[x][measured_variable_position_in_flag_code[
            measured_variable_index_in_list]])  # co2 spike flag_code is in position 5 (i.e. index 4)
        # print(flag_code_str[x][:])

    df = pd.Series(single_flag, index=flag_code_str.index).astype(
        int)  # convert list --> pandas Series, index is the original timestamp, Series is also converted to integer
    bad_data_positions = (df == 1) & (
            y_original != -9999)  # yields TRUE if 1, 1 = bad data; flag can be 9 if test in EddyPro was not selected, ignores data that was already missing
    bad_data_values = y_original[bad_data_positions]
    criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                              2)  # percentage of data lost b/c of this criterion
    y[bad_data_positions] = -9999
    remaining_data_points = len(y[y != -9999])  # counts remaining data points in time series that are not -9999
    print("* after considering " + flag_name + ": " + str(remaining_data_points) + "(" + str(
        np.round((remaining_data_points / y_length_without_missing_values) * 100, 2)) + "% remain)")
    print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

    plot_legend_string.append(flag_code)

    return y, bad_data_values, bad_data_positions, plot_legend_string


def consider_flag_list_2_digits(flag_name, contents, measured_variable_position_in_flag_code,
                                measured_variable_index_in_list, y, y_length_without_missing_values, plot_legend_string,
                                y_original):
    flag_code = contents[flag_name]  # contains flag_code code (and pandas index!)
    flag_code[flag_code == -9999] = 99  # set missing data to 2-digit eddypro flag_code code
    flag_code_str = flag_code.astype(str)  # Series; convert flag_code code to str
    single_flag = []  # list; empty; will only contain the flag for the compound

    for x in range(0, len(flag_code_str)):  # construct complete time series of the single flag; x is the row number
        single_flag.append(flag_code_str[x][measured_variable_position_in_flag_code[measured_variable_index_in_list]])
        # print(flag_code_str[x][:])

    df = pd.Series(single_flag, index=flag_code_str.index).astype(
        int)  # convert list --> pandas Series, index is the original timestamp, Series is also converted to integer
    bad_data_positions = (df == 1) & (
            y_original != -9999)  # yields TRUE if 1, 1 = bad data; flag can be 9 if test in EddyPro was not selected, ignores data that was already missing
    bad_data_values = y_original[bad_data_positions]
    criterion_loss = np.round((len(bad_data_values) / y_length_without_missing_values) * 100,
                              2)  # percentage of data lost b/c of this criterion
    y[bad_data_positions] = -9999
    remaining_data_points = len(y[y != -9999])  # counts remaining data points in time series that are not -9999
    print("* after considering " + flag_name + ": " + str(remaining_data_points) + "(" + str(
        np.round((remaining_data_points / y_length_without_missing_values) * 100, 2)) + "% remain)")
    print("  data loss because of this criterion: " + str(criterion_loss) + "%\n")

    plot_legend_string.append(flag_code)

    return y, bad_data_values, bad_data_positions, plot_legend_string


def drop_cols_from_dataframe(dataframe, *columns_to_remove):
    for column in columns_to_remove:
        if column in dataframe.columns:
            # columns_index += [dataframe.columns.get_loc(column)]  # returns column index
            dataframe = dataframe.drop(dataframe.columns.values[dataframe.columns.get_loc(column)],
                                       1)  # remove object type columns from df
    return dataframe
