import matplotlib as mpl
# mpl.use('Agg')  # when calling savefig() instead of show() the pop-up window won’t appear
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import dates
from matplotlib.ticker import MultipleLocator
import datetime as dt
# from pandas.stats.moments import *
import pandas
import sys


def plot_diurnal_cycles(data_name, data_values, data_units, destination_folder):

    # assemble PNG output
    print("Now plotting daily average cycles...")
    print("working on: " + data_name)
    plot_name = data_name

    # plot_name = plot_name.replace('*', 'star')
    # plot_name = plot_name.replace('/', '_over_')

    y = data_values

    fig = plt.figure(figsize=(11.69, 6.58), dpi=150)
    plt.title(plot_name)

    y = y.replace(-9999, np.nan)  # general missing value
    y = y.replace(-6999, np.nan)  # special missing value used for some scalars, e.g. ch4
    y = y.dropna()

    if not y.empty:

        heading_size = 12
        subheading_size = 10
        label_size = 7
        text_size = 7

        # let's see which months we have in our time series
        y_found_months = y.index.month
        y_found_months = np.unique(y_found_months)
        print("...found data for months: " + str(y_found_months))

        ax = plt.subplot2grid((4, 3), (0, 0))

        # not let's cycle through all found months
        plot_counter = 0
        for month_cycler in range(0, len(y_found_months)):

            print("...calculating month " + str(month_cycler + 1))

            plot_counter += 1

            y_one_month = y[y.index.month == y_found_months[month_cycler]]  # loads y data only from current month

            if 1 <= month_cycler + 1 <= 3:
                plot_row = 0
                plot_column = month_cycler
            elif 4 <= month_cycler + 1 <= 6:
                plot_row = 1
                plot_column = month_cycler - 3
            elif 7 <= month_cycler + 1 <= 9:
                plot_row = 2
                plot_column = month_cycler - 6
            elif 10 <= month_cycler + 1 <= 12:
                plot_row = 3
                plot_column = month_cycler - 9
            else:
                input("Month not valid. Please check file. Stopping program. Sorry.")
                sys.exit()

            # hourly average
            ax1 = plt.subplot2grid((4, 3), (plot_row, plot_column), colspan=1, rowspan=1)
            hourly_avg = y_one_month.groupby(y_one_month.index.hour).mean()
            hourly_std = y_one_month.groupby(y_one_month.index.hour).std()
            hourly_avg.plot()
            plt.fill_between(hourly_avg.index, hourly_avg - hourly_std, hourly_avg + hourly_std, color='k',
                             alpha=0.1)
            ax1.set_xlabel("hour", size=label_size)
            ax1.set_ylabel(data_units, size=label_size)
            ax1.text(0.06, 0.85, str(y_found_months[month_cycler]), horizontalalignment='center',
                     verticalalignment='center', transform=ax1.transAxes, backgroundcolor='#ffc532',
                     size=subheading_size)
            if plot_counter == 1:
                ax1.text(0.05, 1.1, data_name, horizontalalignment='left',
                         verticalalignment='baseline', transform=ax1.transAxes, backgroundcolor='#b1d32f',
                         size=heading_size, )
            ax1.tick_params(axis='both', labelsize=label_size)
            plt.setp(ax1.xaxis.get_majorticklabels(), rotation=0)
            plt.axhline(0, color='black', alpha=0.8)
            plt.xlim(0, 23)
            if hourly_avg.min() < 0:
                factor = 1.2
            else:
                factor = 0.8
            plt.ylim(hourly_avg.min() * factor, hourly_avg.max() * 1.2)
            majorLocator = MultipleLocator(2)
            ax1.xaxis.set_major_locator(majorLocator)

            # # text info output
            # fig.text(0.8, 0.05, y.describe(), size=text_size, backgroundcolor='#CCCCCC')
            # if quality_controlled == 1:
            # plt.figtext(0.84, 0.97, "quality controlled using " + qc_string, color='red', size=text_size)

        # plt.show()
        # plt.tight_layout()
        # fig.subplots_adjust(hspace=0.1)
        print("...saving plots to PNG image...")
        plot_name = os.path.join(destination_folder, plot_name)
        plt.savefig(plot_name + '_daily_average_cycles.png', dpi=150)
        plt.close()

    else:
        print("Data for " + data_name + " is empty --> no plot")